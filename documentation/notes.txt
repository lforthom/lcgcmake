For ec-ubuntu-14-04-x86-64-1 the following packages have been installed:

   1) OpenGL----------------------------------------------(needed for coin3d)
   2) libtool---------------------------------------------(needed for libunwind)
   3) uuid-dev, libuuid-devel-----------------------------(needed for xapian) 
   4) libxml2-dev and libxslt1-dev------------------------(needed for python)
   5) libbz2-dev------------------------------------------(needed for boost)