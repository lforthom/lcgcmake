cmake_minimum_required(VERSION 2.8 FATAL_ERROR)
project(LCGSoft C CXX Fortran)

# Global configuration number
set (GLOBAL_REVISION_NUM "")

list(INSERT CMAKE_MODULE_PATH 0 ${CMAKE_SOURCE_DIR}/cmake/modules)
if(NOT CMAKE_CONFIGURATION_TYPES AND NOT CMAKE_BUILD_TYPE) # Default to a Release build
  set(CMAKE_BUILD_TYPE Release CACHE STRING "Choose the type of build [Release MinSizeRel Debug RelWithDebInfo]" FORCE)
endif()

#---Definition options and teir default values ------------------------------------------------------------------------
set(LCG_INSTALL_POLICY separate CACHE STRING "'separate' or 'join' installation of packages")
set(LCG_SAFE_INSTALL   OFF      CACHE BOOL "Ensure that no overwites occurs at the installation area of packages")
if (VALIDATION)
  set(LCG_IGNORE         "pythia8;pythia6;agile;rivet;herwig++"       CACHE STRING "List of packages to be ignored from LCG_INSTALL_PREFIX (';' separated)")
else()
  set(LCG_IGNORE         ""       CACHE STRING "List of packages to be ignored from LCG_INSTALL_PREFIX (';' separated)")
endif()
set(LCG_INSTALL_PREFIX ""       CACHE PATH "Existing LCG installation path prefix.")
set(LCG_TARBALL_INSTALL   OFF   CACHE BOOL "Turn ON/OFF creation/installation of tarballs")
set(LCG_SOURCE_INSTALL    ON    CACHE BOOL "Turn ON/OFF installation of sources in /share")
set(VALIDATION            OFF   CACHE BOOL "Enable validation settings.")
set(POST_INSTALL          ON    CACHE BOOL "Enable validation settings.")
set(STRIP_RPATH           ON    CACHE BOOL "Strip RPATH from binaries.")
string(REPLACE " " ";" LCG_IGNORE "${LCG_IGNORE}")

#---Report the values of the options-----------------------------------------------------------------------------------
message(STATUS "Target installation prefix             : ${CMAKE_INSTALL_PREFIX}")
message(STATUS "Existing LCG installations             : ${LCG_INSTALL_PREFIX}")
message(STATUS "Safe installation                      : ${LCG_SAFE_INSTALL}")
message(STATUS "Source and binary tarball installation : ${LCG_TARBALL_INSTALL}")
message(STATUS "Source installation in /share          : ${LCG_SOURCE_INSTALL}")
message(STATUS "Validation mode                        : ${VALIDATION}")
message(STATUS "Preparing .post-install.sh scripts     : ${POST_INSTALL}")
message(STATUS "Stripping RPATH from binaries          : ${STRIP_RPATH}")
message(STATUS "Ignored packages from LCG installation : ${LCG_IGNORE}")

include(${CMAKE_SOURCE_DIR}/toolchain.cmake)
include(lcgsoft-macros)

#---Define Global variables--------------------------------------------------------------------------------------------
find_program(env_cmd NAMES env)
mark_as_advanced(env_cmd)
set(EXEC ${env_cmd})
set(MAKE ${CMAKE_SOURCE_DIR}/cmake/scripts/make_verbose $(MAKE))
set(LOCKFILE ${CMAKE_SOURCE_DIR}/cmake/scripts/lockfile.py)
#set(GenURL http://service-spi.web.cern.ch/service-spi/external/tarFiles)
set(GenURL http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/sources) 
set(PYTHON python${Python_version_major})

#--- Select the vector instruction set --------------------------------------------------------------------------------
if(LCG_ARCH MATCHES "x86_64")
  set(LCG_INSTRUCTIONSET "sse4.2")
else()
  set(LCG_INSTRUCTIONSET ${LCG_ARCH})
endif()
message (STATUS "LCG_INSTRUCTIONSET                     : ${LCG_INSTRUCTIONSET}")

string(TOUPPER "${CMAKE_BUILD_TYPE}" _build_type_upper)
if (_build_type_upper)
  set (_flag_suffix "_${_build_type_upper}")
endif()
message (STATUS "LCG_CPP11                              : ${LCG_CPP11}")
message (STATUS "LCG_CPP1Y                              : ${LCG_CPP1Y}")
message (STATUS "LCG_CPP14                              : ${LCG_CPP14}")

if(LCG_CPP14)
  set(CMAKE_CXX_FLAGS ${CMAKE_CXX_FLAGS} -std=c++14)
elseif(LCG_CPP1Y)
  set(CMAKE_CXX_FLAGS ${CMAKE_CXX_FLAGS} -std=c++1y)
elseif(LCG_CPP11)
  set(CMAKE_CXX_FLAGS ${CMAKE_CXX_FLAGS} -std=c++11)
endif()

if(LCG_EXT_ARCH)
  set(CMAKE_CXX_FLAGS ${CMAKE_CXX_FLAGS} +$ENV{CXXOPTIONS})
endif()

message (STATUS "Common compiler flags: ")
foreach (_compiler C CXX Fortran)
  message(STATUS "  ${_compiler} : ${CMAKE_${_compiler}_FLAGS} ${CMAKE_${_compiler}_FLAGS${_flag_suffix}}")
endforeach()

# defines LIBRDIR variable to be either lib or lib64
set(LIBDIR_DEFAULT lib)
if(CMAKE_SYSTEM_NAME MATCHES "Linux" AND NOT CMAKE_CROSSCOMPILING AND NOT EXISTS "/etc/debian_version")
  if("${CMAKE_SIZEOF_VOID_P}" EQUAL "8")
    set(LIBDIR_DEFAULT lib64)
  endif()
endif()

# define Fortran RT library
execute_process(COMMAND ${CMAKE_Fortran_COMPILER} -print-file-name=libgfortran${CMAKE_SHARED_LIBRARY_SUFFIX}
                        OUTPUT_VARIABLE FORTRAN_LIBRARY
                        OUTPUT_STRIP_TRAILING_WHITESPACE)
get_filename_component(FORTRAN_LIBRARY_DIR ${FORTRAN_LIBRARY} DIRECTORY)

#---Enable (the defintion) of tests by default-------------------------------------------------------------------------
enable_testing()
include(CTest)

set(TESTLOGDIR "${PROJECT_BINARY_DIR}/tests")
if(NOT "$ENV{CXXFLAGS}" STREQUAL "")
  set(libtoolpatch CXX=$ENV{CXX}\ $ENV{CXXFLAGS})
endif()

file(MAKE_DIRECTORY ${CMAKE_BINARY_DIR}/timestamps)

set(MakeSitePackagesDir ${CMAKE_COMMAND} -E  make_directory <INSTALL_DIR>/lib/python${Python_config_version_twodigit}/site-packages)

add_subdirectory(externals)
add_subdirectory(gridexternals)
add_subdirectory(projects)
add_subdirectory(pyexternals)
add_subdirectory(generators)

if (VALIDATION)
  add_custom_target(validation.pre_builds DEPENDS rivet ROOT lhapdf Python HepMC agile cmaketools)
  add_custom_target(validation.pre_tests COMMAND ctest -R lhapdf6sets.download COMMAND ctest -R rivet-tests.genser-prepare_references COMMAND ctest -R rivet-tests.genser-buildanalysis DEPENDS validation.pre_builds)
  foreach(pkg sherpa pythia8 pythia6 herwig herwig++)
    set (_deplist)
    foreach(v ${${pkg}_native_version})
      add_custom_target(validation.${pkg}-${v} COMMAND ${CMAKE_SOURCE_DIR}/generators/validation/run.sh ${pkg} ${v} DEPENDS ${pkg}-${v} validation.pre_tests)
      list (APPEND _deplist validation.${pkg}-${v})
    endforeach()
    add_custom_target(validation.${pkg} DEPENDS ${_deplist})
    list(APPEND _depglobal validation.${pkg})
  endforeach()
  add_custom_target(validation DEPENDS ${_depglobal})
  add_custom_target(validation.publish COMMAND ${CMAKE_SOURCE_DIR}/generators/validation/publish.sh)
endif()

LCG_create_dependency_files()
file(WRITE ${CMAKE_BINARY_DIR}/fail-logs.txt "")

#---Target to create a LCG view in the install prefix area
add_custom_target(view
                  COMMAND ${CMAKE_SOURCE_DIR}/cmake/scripts/create_lcg_view.py
                  -l ${CMAKE_INSTALL_PREFIX} -p ${LCG_system} -d -B ${CMAKE_INSTALL_PREFIX}/${LCG_system})
